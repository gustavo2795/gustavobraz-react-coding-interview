import React, { useState, useRef, useEffect } from 'react'
import { Typography, Box } from '@mui/material'
import { Input as AtomicInput } from '@components/atoms'
import { Button } from '@components/atoms'

interface InputProps {
    placeholder?: string
    value: string
    typography?: any
    textColor?: string
}

function Input({
    value, 
    typography = 'body1', 
    textColor
  }: InputProps) {
    const refOpenInput = useRef(null)
    const [isEditable, setIsEditable] = useState<boolean>(false)
    const [inputvalue, setInputValue] = useState<string>(value)
    const initialValue = value

    useEffect(() => {
        const handleClickOutside = (event: MouseEvent) => {
          if (refOpenInput.current && !refOpenInput.current.contains(event.target)) {
            setIsEditable(false);
          }
        };
        document.addEventListener('click', handleClickOutside, true);
        return () => {
          document.removeEventListener('click', handleClickOutside, true);
        };
      }, []); 

    return (
        <label  onClick={() => setIsEditable(true)} >
            {isEditable ? (
              <Box display="flex" flexDirection="column">
                <AtomicInput
                  ref={refOpenInput}
                  onChange={(e) => setInputValue(e.target.value)}
                  value={inputvalue}
                />
                <Box display="flex" justifyContent="flex-end">
                  <Button onClick={() => console.log('close')}>Ok</Button>
                  <Button onClick={() => setInputValue(initialValue)}>X</Button>
                </Box>
              </Box>
            ) : (
                <Typography 
                  color={textColor}
                  variant={typography}
                >
                  {inputvalue ?? initialValue}
                </Typography>
            )}
        </label>
    
  )
}

export default Input