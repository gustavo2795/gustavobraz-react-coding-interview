import { Box } from '@mui/material';

export const ListContainer: React.FC<{ children: React.ReactNode }> = ({
    children,
  }) => (
    <Box
      sx={{
        display: 'grid',
        gridTemplateColumns: 'repeat(2, 1fr)',
        gap: 2,
      }}
    >
      {children}
    </Box>
  );