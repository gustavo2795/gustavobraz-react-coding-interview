import { Input as MUIInput, styled } from '@mui/material';

export const Input = styled(MUIInput)(({ theme }) => ({
  position: 'relative',
  background: '#FFF',
  boxShadow: theme.shadows[2],
  borderRadius: 8,
  minWidth: '100px'
}));
