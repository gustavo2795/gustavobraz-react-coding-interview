import { styled } from "@mui/material";

export const LogoImg = styled('img')(() => ({
    height: '32px',
}));