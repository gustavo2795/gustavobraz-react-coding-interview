import { Box, styled } from '@mui/material';

export const Wrapper = styled(Box)(({ theme }) => ({
    position: 'fixed',
    zIndex: 1000,
    background: '#FFF',
    height: '64px',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    padding: '12px 24px',
    boxShadow: theme.shadows[1],
  }));