import { styled } from '@mui/material';

export const Button = styled('button')(({ theme }) => ({
  width: '30px',
  height: '30px',
  background: 'transparent',
  borderColor: '#0E83DD',
  borderRadius: '3px'
}));
